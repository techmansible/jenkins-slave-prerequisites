## Jenkins_slave_install:
    Download the binaries of git,gradle and java from artifactory
    Untar the binaries git,gradle and java
    Installs the prerequisties(git, gradle, java)of jenkins-slave 

## Variables that can be passed dynamically during playbook execution

    run_as                        - User to run the playbook                                                    
    jenkins_master_id_rsa         - Use the public key that needs to be copied in jenkins-slave                          
    jenkins_slave_username        - Username of jenkins-slave
    gradle_binary_main_directory  - Main directory of gradle configuration
    
## Mandate variable for SSH key installation
    
    jenkins_master_id_rsa    - Specify the master public key that need to be copied into jenkins slave machine